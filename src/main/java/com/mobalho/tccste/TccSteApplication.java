package com.mobalho.tccste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TccSteApplication {

	public static void main(String[] args) {
		SpringApplication.run(TccSteApplication.class, args);
	}
}
